package org.mal.ascold.fragment.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.mal.ascold.R;

import java.util.ArrayList;

/**
 * Created by Dmitry.Subbotenko on 18.08.2016.
 */
public class MainMenuAdapter extends RecyclerView.Adapter<MainMenuItemViewHolder> {


  private ArrayList<MainMenuItem> mainMenu;

  public MainMenuAdapter(ArrayList<MainMenuItem> mainMenu) {
    this.mainMenu = mainMenu;
  }

  @Override
  public MainMenuItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_menu, parent, false);

    MainMenuItemViewHolder vh = new MainMenuItemViewHolder(v);

    return vh;
  }

  @Override
  public void onBindViewHolder(MainMenuItemViewHolder holder, int position) {
    MainMenuItem menuItem = mainMenu.get(position);
    holder.setUp(menuItem);
  }

  @Override
  public int getItemCount() {
    return mainMenu.size();
  }



}
