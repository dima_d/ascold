package org.mal.ascold.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import org.mal.ascold.R;
import org.mal.ascold.activity.MainActivity;
import org.mal.ascold.fragment.adapter.MainMenuAdapter;

/**
 * Created by Dmitry.Subbotenko on 19.08.2016.
 */
public class FullscreenTextFragment extends Fragment{
  @BindView(R.id.fullscreenText)
  TextView fullscreenText;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_main, container, false);
    ButterKnife.bind(view);

    fullscreenText.setText(((MainActivity)getActivity()).fullScreenText);


    return view;
  }
}
