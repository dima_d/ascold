package org.mal.ascold.fragment.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.androidquery.AQuery;
import org.mal.ascold.R;
import rx.Observable;
import rx.subjects.BehaviorSubject;

/**
 * Created by Dmitry.Subbotenko on 17.08.2016.
 */
public class LoginDialogFragment extends DialogFragment {


  BehaviorSubject<Pair<CharSequence, CharSequence>> observable = BehaviorSubject.create();

  public Observable<Pair<CharSequence, CharSequence>> getObservable() {
    return observable;
  }

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

    LayoutInflater inflater = getActivity().getLayoutInflater();

    View view = inflater.inflate(R.layout.dialog_signin, null);
    AQuery aq = new AQuery(view);
    builder.setView(view)
        .setPositiveButton(R.string.signin, (dialog, id) -> {
          observable.onNext(new Pair<>(aq.id(R.id.username).getText(),aq.id(R.id.password).getText()));
          observable.onCompleted();
        })
        .setNegativeButton(R.string.cancel, (dialog, id) -> {
          observable.onCompleted();
        });
    return builder.create();
  }

}
