package org.mal.ascold.fragment.adapter;

import rx.functions.Action1;
import rx.subjects.PublishSubject;

/**
 * Created by Dmitry.Subbotenko on 18.08.2016.
 */
public class MainMenuItem {
  private String title;
  private String summary;
  private int imageId;
  private PublishSubject subject = PublishSubject.create();

  public MainMenuItem(String title, String summary, int imageId) {
    this.title = title;
    this.summary = summary;
    this.imageId = imageId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public MainMenuItem subscribeClick(final Action1 onClick) {
    subject.subscribe(onClick);
    return this;
  }

  public PublishSubject getSubject() {
    return subject;
  }

  public int getImageId() {
    return imageId;
  }
}
