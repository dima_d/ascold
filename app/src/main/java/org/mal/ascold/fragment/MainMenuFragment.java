package org.mal.ascold.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import org.mal.ascold.R;
import org.mal.ascold.activity.IntroActivity;
import org.mal.ascold.activity.MainActivity;
import org.mal.ascold.applets.QrReader;
import org.mal.ascold.fragment.adapter.MainMenuAdapter;
import org.mal.ascold.fragment.adapter.MainMenuItem;
import org.mal.ascold.service.MvelService;
import org.mvel2.MVEL;
import rx.Observable;
import rx.subjects.BehaviorSubject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;

/**
 * Created by Dmitry.Subbotenko on 18.08.2016.
 */
public class MainMenuFragment extends Fragment {
  private static final String TAG = MainMenuFragment.class.getSimpleName();

  private ArrayList<MainMenuItem> mainMenu = new ArrayList<MainMenuItem>() {{
    add(new MainMenuItem("Вводная", "Показать вводную.", R.drawable.txt_file_icon)
        .subscribeClick(v -> onIntroductionClick()));
    add(new MainMenuItem("Данные игрока", "Показать сводную таблицу данных игрока.", R.drawable.list_icon)
        .subscribeClick(v -> onDataClick()));
    add(new MainMenuItem("Сканер", "Включить камеру для сканирования QR-кода.", R.drawable.qr_icon)
        .subscribeClick(v -> onQRClick()));
    add(new MainMenuItem("Синхронизация", "Синхронизовать данные c сервером.", R.drawable.ic_launcher)
        .subscribeClick(v -> onSyncClick()));
  }};
  private QrReader qrReader;
  private Observable<MvelService> mvelService;

  private void onQRClick() {
    Log.d(TAG, "onQRClick() called with " + "");
    qrReader.scan(getActivity());
  }

  private void onSyncClick() {
//
//    try {
//      FileReader reader = new FileReader(getActivity().getAssets().openFd("Scripts.txt").getFileDescriptor());
//
//      Writer writer = new StringWriter();
//
//      char[] buffer = new char[2048];
//      int n;
//      while ((n = reader.read(buffer)) != -1) {
//        writer.write(buffer, 0, n);
//      }
//      String script = writer.toString();
//      reader.close();
//
//    } catch (IOException e) {
//      e.printStackTrace();
//    }

    mvelService = ((MainActivity) getActivity()).mvelService;
    mvelService.subscribe(service -> {
      service.addScript("       Log.d(\"MVEL\", \"onSyncClick() called with \");\n ");

    });



  }

  private void onDataClick() {

  }

  private void onIntroductionClick() {
    startActivity(new Intent(getActivity(), IntroActivity.class));
  }

  @BindView(R.id.recyclerView)
  RecyclerView recyclerView;

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    qrReader.onActivityResult(requestCode, resultCode, data);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_main, container, false);
    ButterKnife.bind(view);

    recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    recyclerView.setAdapter(new MainMenuAdapter(mainMenu));

    qrReader = new QrReader();

    return view;
  }
}
