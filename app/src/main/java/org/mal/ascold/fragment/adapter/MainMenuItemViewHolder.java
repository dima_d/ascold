package org.mal.ascold.fragment.adapter;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.androidquery.AQuery;
import org.mal.ascold.R;

/**
 * Created by Dmitry.Subbotenko on 18.08.2016.
 */
public class MainMenuItemViewHolder extends RecyclerView.ViewHolder {

  private final AQuery aq;

  public MainMenuItemViewHolder(View itemView) {
    super(itemView);
    aq = new AQuery(itemView);

  }

  public void setUp(MainMenuItem item) {
    aq.clicked(v -> {
      item.getSubject().onNext(null);
    });

    aq.id(R.id.titleTextView).text(item.getTitle());
    aq.id(R.id.summaryTextView).text(item.getSummary());
    aq.id(R.id.iconView).image(item.getImageId());

  }
}
