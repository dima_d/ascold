package org.mal.ascold.applets;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.google.zxing.client.android.CaptureActivity;


/**
 * Created by Dmitry.Subbotenko on 14.09.2016.
 */
public class QrReader {
  //  PublishSubject<QrIntent> subject = PublishSubject.create();
  private static final String TAG = QrReader.class.getSimpleName();

  public void scan(Activity activity) {
    //    IntentIntegrator integrator = new IntentIntegrator(activity);
    //    integrator.initiateScan();
    Intent intent = new Intent(activity.getApplicationContext(), CaptureActivity.class);
    intent.setAction("com.google.zxing.client.android.SCAN");
    intent.putExtra("SAVE_HISTORY", false);
    activity.startActivityForResult(intent, 0);

    


  }

  /**
   * onActivity result integration.
   *
   * @param requestCode
   * @param resultCode
   * @param data
   */
  public void onActivityResult(int requestCode, int resultCode, Intent data) {

    //   IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
    //    if (scanningResult != null) {
    //      String scanContent = scanningResult.getContents();
    ////      subject.onNext(new QrIntent(scanContent));
    //    }
    if (requestCode == 0) {
      if (resultCode == Activity.RESULT_OK) {
        String contents = data.getStringExtra("SCAN_RESULT");
        Log.d(TAG, "contents: " + contents);
      } else if (resultCode == Activity.RESULT_CANCELED) {
        // Handle cancel
        Log.d(TAG, "RESULT_CANCELED");
      }
    }

  }

  //  public PublishSubject<QrIntent> getSubject() {
  //    return subject;
  //  }
}
