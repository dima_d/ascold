package org.mal.ascold.activity;

import android.app.Activity;
import android.os.Bundle;

import butterknife.BindView;
import butterknife.ButterKnife;
import org.mal.ascold.R;
import org.mal.ascold.service.JoinRpgService;
import org.mal.ascold.service.net.IJoinRpgService;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import us.feras.mdv.MarkdownView;

/**
 * Created by Dmitry.Subbotenko on 19.08.2016.
 */
public class IntroActivity extends Activity {
  @BindView(R.id.introView)
  MarkdownView introView;
  private Observable<JoinRpgService> joinRpgService;
  private JoinRpgService.Connector joinRpgServiceConnector;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.intro_activity);
    ButterKnife.bind(this);


  }

  @Override
  protected void onStart() {
    super.onStart();
    joinRpgServiceConnector = new JoinRpgService.Connector(this);
    joinRpgService = joinRpgServiceConnector.getService();

    joinRpgService.subscribe(s->s.getIntro().subscribe(i->{
      setTitle(i.getRoleName());
      introView.loadMarkdown(i.getIntroduction());
    }));


  }

  @Override
  protected void onStop() {
    super.onStop();
    joinRpgServiceConnector.getBehavior().onCompleted();
  }



}
