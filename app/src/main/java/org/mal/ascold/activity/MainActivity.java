package org.mal.ascold.activity;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.ButterKnife;
import org.mal.ascold.R;
import org.mal.ascold.fragment.MainMenuFragment;
import org.mal.ascold.fragment.dialog.LoginDialogFragment;
import org.mal.ascold.service.JoinRpgService;
import org.mal.ascold.service.MvelService;
import org.mal.ascold.service.net.IJoinRpgService;
import rx.Observable;
import rx.subjects.BehaviorSubject;

public class MainActivity extends Activity {
  public static final String TAG_LOGIN_DIALOG = "login";
  private static final String TAG = MainActivity.class.getSimpleName();
  public static final String USERNAME = "username";
  public static final String PASSWORD = "password";
  Fragment currentFragment;
  public String fullScreenText;
  private Observable<JoinRpgService> joinRpgService;
  public Observable<MvelService> mvelService;
  private JoinRpgService.Connector joinRpgServiceConnector;
  private MvelService.Connector mvelServiceConnector;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    loadFragment(MainMenuFragment.class, "content");

  }

  @Override
  protected void onStart() {
    super.onStart();
    joinRpgServiceConnector = new JoinRpgService.Connector(this);
    joinRpgService = joinRpgServiceConnector.getService();
    mvelServiceConnector = new MvelService.Connector(this);
    mvelService = mvelServiceConnector.getService();

    login();

  }

  @SuppressWarnings("unchecked")
  private <T extends Fragment> T loadFragment(Class<T> fragmentClass, String tag) {
    T fragment;
    try {
      fragment = (T) getFragmentManager().findFragmentByTag(tag);
    } catch (ClassCastException e) {
      e.printStackTrace();
      return null;
    }
    if (fragment == null) {
      try {
        fragment = fragmentClass.newInstance();
      } catch (InstantiationException e) {
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      }
      getFragmentManager().beginTransaction()
          //            .setCustomAnimations(android.support.v7.appcompat.R.anim.abc_fade_in, R.anim.fade_out)
          .add(R.id.contentView, fragment, tag)
          .commit();
    }

    FragmentTransaction transaction = getFragmentManager().beginTransaction();
    //    transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
    if (currentFragment != null) {
      transaction.detach(currentFragment);
    }
    transaction.attach(fragment);
    transaction.commit();
    currentFragment = fragment;
    return fragment;
  }

  private void login() {
    joinRpgService
        .subscribe(service -> {
          SharedPreferences settings = getSharedPreferences(getPackageName(), 0);
          String username = settings.getString(USERNAME, "");
          String password = settings.getString(PASSWORD, "");

          if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            showFullScreenText(getString(R.string.login_to_joinrpg));
            return;
          }

          showFullScreenText(getString(R.string.please_wait_login));

          service.login(username, password).subscribe(authorization -> {

            //showFullScreenText("Вы вошли как "+ authorization.getName());
            //                  flipper.setDisplayedChild(1);

          }, e -> {
            showFullScreenText("Неверный логин или пароль");
          });
        });
  }

  private void showFullScreenText(String string) {
    fullScreenText = string;
    //    flipper.setDisplayedChild(0);
    //    fullscreenText.setText(string);
  }

  @Override
  protected void onStop() {
    super.onStop();
    joinRpgServiceConnector.getBehavior().onCompleted();
    mvelServiceConnector.getBehavior().onCompleted();
//    joinRpgService.onCompleted();
  }


  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id) {
    case R.id.action_login:
      LoginDialogFragment newFragment = new LoginDialogFragment();
      newFragment.getObservable()
          .subscribe(pair -> {
            SharedPreferences settings = getSharedPreferences(getPackageName(), 0);
            settings.edit().putString(USERNAME, pair.first.toString()).putString(PASSWORD, pair.second.toString())
                .commit();
            login();

          });
      newFragment.show(getFragmentManager(), TAG_LOGIN_DIALOG);

      return true;
    case R.id.action_settings:

      return true;
    default:
      break;
    }

    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

}
