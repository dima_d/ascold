package org.mal.ascold.utils;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import rx.Observable;
import rx.subjects.BehaviorSubject;

/**
 * Created by Dmitry.Subbotenko on 17.10.2016.
 */
public class ServiceConnector<T extends BoundService> {

  private BehaviorSubject<T> serviceSubject = BehaviorSubject.create();
  private BoundServiceConnection boundServiceConnection = new BoundServiceConnection();

  public ServiceConnector(Context context, Class<T> tClass) {

    Intent orderServiceIntent = new Intent(context, tClass);
    context.bindService(orderServiceIntent,
        boundServiceConnection, Context.BIND_AUTO_CREATE);

    serviceSubject.doOnError(t -> t.printStackTrace()).subscribe(s -> {
    }, t -> {
    }, () -> context.unbindService(boundServiceConnection));
  }

  private class BoundServiceConnection implements ServiceConnection {
    @Override
    public void onServiceConnected(ComponentName name, IBinder binder) {
      Service service = ((ServiceBinder) binder).getService();
      serviceSubject.onNext((T) service); // May produce Class cast Exception
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
      serviceSubject.onCompleted();
    }
  }

  public Observable<T> getService() {
    return serviceSubject.first();
  }

  public BehaviorSubject<T> getBehavior() {
    return serviceSubject;
  }
}