package org.mal.ascold.utils;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Service base class to support binding.
 */
public abstract class BoundService extends Service {

  private IBinder binder = new ServiceBinder(this);

  @Override
  public IBinder onBind(Intent intent) {
    return binder;
  }
}