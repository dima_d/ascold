package org.mal.ascold.utils;

import android.os.Binder;
/**
 * Binder for accessing Android application services.
 */
public class ServiceBinder extends Binder {

  private BoundService service;

  /**
   * Constructor.
   *
   * @param service
   */
  public ServiceBinder(BoundService service) {
    this.service = service;
  }

  /**
   * Returns binded service.
   *
   * @return
   */
  public BoundService getService() {
    return service;
  }
}