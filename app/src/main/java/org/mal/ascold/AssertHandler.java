/*
 *
 * =======================================================================
 *
 * Copyright (c) 2014-2015 Domlex Limited. All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Domlex Limited.
 * You shall not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into with
 * Domlex Limited.
 *
 * =======================================================================
 *
 */

package org.mal.ascold;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.MessageQueue;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.concurrent.CountDownLatch;

/**
 * Class for showing assert windows.
 * Use assert*() and fail() methods for unrecoverable conditions
 * In debug builds they would show dialog, in release build - throw assertion error
 * Use check*() methods for recoverable conditions.
 * In debug builds they would show dialog, in release build - only log error to logcat
 */
public class AssertHandler {

  protected static final String TAG = AssertHandler.class.getSimpleName();

  public static final String DEBUG = "debug";
  public static final String RELEASE = "release";
  public static final String WORK_MODE = DEBUG;

  private static Method mMsgQueueNextMethod = null;
  private static Field mMsgTargetFiled = null;
  private static boolean isQuitModal = false;
  private static HashSet<String> ignoredSet = new HashSet<String>();
  private static Context context;

  private AssertHandler() {
    // Static use only
  }

  public static void init(Context context){

    AssertHandler.context = context;
  }


  /**
   * Asserts that an object isn't null. If it is AssertDialog would be shown.
   *
   * @param object Object to check or {@code null}
   * @param info   the identifying message
   */
  public static void assertNonNull(@Nullable Object object, @Nullable String info) {
    if (object == null) {
      showAssert(info);
    }
  }

  /**
   * Asserts that two objects are equal. If they are not, an
   * AssertDialog is shown with the given message. If
   * {@code expected} and {@code actual} are {@code null},
   * they are considered equal.
   *
   * @param expected expected value
   * @param actual   actual value
   * @param info     the identifying message
   */
  public static void assertEquals(@Nullable Object expected, @Nullable Object actual, @Nullable String info) {
    if (equalsRegardingNull(expected, actual)) {
      return;
    }
    showAssert(info);
  }

  /**
   * Asserts that two objects are <b>not</b> equal. If they are, an
   * AssertDialog is shown with the given message. If
   * {@code unexpected} and {@code actual} are {@code null},
   * they are considered equal.
   *
   * @param unexpected expected value
   * @param actual     actual value
   * @param info       the identifying message
   */
  public static void assertNotEquals(@Nullable Object unexpected, @Nullable Object actual, @Nullable String info) {
    if (!equalsRegardingNull(unexpected, actual)) {
      return;
    }
    showAssert(info);
  }

  /**
   * Asserts that a condition is true. If it isn't it AssertDialog would be shown.
   *
   * @param condition condition to be checked
   * @param info      the identifying message
   */
  public static void assertTrue(boolean condition, @Nullable String info) {
    if (!condition) {
      showAssert(info);
    }
  }

  /**
   * Asserts that a condition is false. If it isn't it AssertDialog would be shown.
   *
   * @param condition condition to be checked
   * @param info      the identifying message
   */
  public static void assertFalse(boolean condition, @Nullable String info) {
    if (condition) {
      showAssert(info);
    }
  }

  /**
   * Show Assert dialog and wait for user input.
   *
   * @param message the identifying message
   */
  public static void showAssert(@Nullable final String message) {
    showAssert(message, null);
  }

  /**
   * Show Assert dialog and wait for user input.
   *
   * @param message   the identifying message
   * @param throwable throable to log
   */
  public static void showAssert(@Nullable final String message, @Nullable Throwable throwable) {
    switch (WORK_MODE) {
    case DEBUG:
      showAssertInner(message, throwable);
      break;
    case RELEASE:
      // Ignore result
      formatAndlogMessage(message, throwable);
      break;
    default:
      throw new IllegalStateException("Unknown mode: " + WORK_MODE);
    }
  }

  /**
   * Asserts that an object isn't null. If it is AssertDialog would be shown.
   * Caues crash in release builds.
   *
   * @param object Object to check or {@code null}
   * @param info   the identifying message
   */
  
  public static void checkNonNull(@Nullable Object object, @Nullable String info) {
    if (object == null) {
      crash(info);
    }
  }

  /**
   * Asserts that two objects are equal. If they are not, an
   * AssertDialog is shown with the given message. If
   * {@code expected} and {@code actual} are {@code null},
   * they are considered equal.
   * Causes crash in release builds.
   *
   * @param expected expected value
   * @param actual   actual value
   * @param info     the identifying message
   */
  public static void checkEquals(@Nullable Object expected, @Nullable Object actual, @Nullable String info) {
    if (equalsRegardingNull(expected, actual)) {
      return;
    }
    crash(info);
  }

  /**
   * Asserts that a condition is true. If it isn't it AssertDialog would be shown.
   * Causes crash in release builds.
   *
   * @param condition condition to be checked
   * @param info      the identifying message
   */
  
  public static void checkTrue(boolean condition, @Nullable String info) {
    if (!condition) {
      crash(info);
    }
  }

  /**
   * Show Assert dialog and wait for user input.
   * Causes crash in release builds.
   *
   * @param message the identifying message
   */
  
  public static void crash(@Nullable String message) {
    crash(message, null);
  }

  /**
   * Show Assert dialog and wait for user input.
   * Causes crash in release builds.
   *
   * @param message   the identifying message
   * @param throwable throable to log
   */
  
  public static void crash(@Nullable String message, @Nullable Throwable throwable) {
    switch (WORK_MODE) {
    case DEBUG:
      showAssertInner(message, throwable);
      break;
    case RELEASE:
      throw new Error(message, throwable);
    default:
      throw new IllegalStateException("Unknown mode: " + WORK_MODE);
    }
  }

  private static boolean equalsRegardingNull(Object expected, Object actual) {
    if (expected == null) {
      return actual == null;
    }

    return isEquals(expected, actual);
  }

  private static boolean isEquals(Object expected, Object actual) {
    return expected.equals(actual);
  }

  private static String formatAndlogMessage(@Nullable String message, @Nullable Throwable throwable) {
    String logMessage = "";
    // If we were passed with null message, try to take it from error
    if (TextUtils.isEmpty(message)) {
      if (throwable != null) {
        if (throwable.getMessage() != null) {
          logMessage = throwable.getMessage();
        } else {
          logMessage = throwable.getClass().getSimpleName();
        }
      }
      // Both message and throable are null
    } else {
      logMessage = message;
    }
    Log.wtf(TAG, "ASSERT: " + logMessage, (throwable != null) ? throwable : new Throwable());
    return logMessage;
  }

  private static void showAssertInner(@Nullable final String info,
      @Nullable Throwable throwable) {
    final String title = formatAndlogMessage(info, throwable);

    // If user chose to ignore this assert, don't do anything.
    if (ignoredSet.contains(title)) {
      return;
    }

    if (!prepareModal()) {
      throw new RuntimeException("Failed to show assert");
    }

    final CountDownLatch latch = new CountDownLatch(1);
    final boolean isUiThread = Looper.getMainLooper().getThread() == Thread.currentThread();
    Runnable runnable = new Runnable() {

      @Override
      public void run() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context,
            AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builder.setTitle("ASSERT!");
        builder.setMessage(title);
        builder.setCancelable(false);
        builder.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            isQuitModal = true;
            Log.wtf(TAG, "ASSERT CONTINUE");
            dialog.dismiss();
            latch.countDown();
          }
        });
        builder.setNegativeButton("Stop", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            dialog.dismiss();
            if (isUiThread) {
              // In case we are on UI thread, just throw an Error, it should crash the app
              Log.wtf(TAG, "ASSERT STOP");
              throw new AssertionError();
            } else {
              // Throwing an error won't help, it would just stop the thread. Kill whole app.
              Log.wtf(TAG, "ASSERT STOP");
              System.exit(1);
            }
          }
        });
        builder.setNeutralButton("Ignore", new DialogInterface.OnClickListener() {

          @Override
          public void onClick(DialogInterface dialog, int which) {
            ignoredSet.add(info);
            isQuitModal = true;
            Log.wtf(TAG, "ASSERT IGNORE");
            dialog.dismiss();
            latch.countDown();
          }
        });

        AlertDialog alert = builder.create();
        alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alert.show();

        doModal();
      }
    };

    if (isUiThread) {
      runnable.run();
    } else {
      new Handler(Looper.getMainLooper()).post(runnable);
      try {
        latch.await();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

//  /**
//   * Show our fancy gradient screen of death.
//   *
//   * @param context context to show dialog in
//   * @param ex      exception to display
//   */
//
//  public static void showGsod(@NonNull final Context context, @NonNull final Throwable ex) {
//    Log.wtf(TAG, "GSOD", ex);
//
//    CrashLogProvider provider = new CrashLogProvider();
//    provider.saveLog();
//
//    if (!prepareModal()) {
//      return;
//    }
//
//    final CountDownLatch latch = new CountDownLatch(1);
//    final boolean isUiThread = Looper.getMainLooper().getThread() == Thread.currentThread();
//    final Runnable runnable = new Runnable() {
//
//      @Override
//      public void run() {
//        final Dialog dialog = new GsodDialog(context, ex);
//        dialog.show();
//        // We don't expect to ever exit from inner loop
//        doModal();
//      }
//    };
//
//    if (isUiThread) {
//      runnable.run();
//    } else {
//      new Handler(Looper.getMainLooper()).post(runnable);
//      try {
//        latch.await();
//      } catch (InterruptedException e) {
//        e.printStackTrace();
//      }
//    }
//  }

  private static boolean prepareModal() {
    Class<?> clsMsgQueue;
    Class<?> clsMessage;

    try {
      clsMsgQueue = Class.forName("android.os.MessageQueue");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
      return false;
    }

    try {
      clsMessage = Class.forName("android.os.Message");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
      return false;
    }

    try {
      mMsgQueueNextMethod = clsMsgQueue.getDeclaredMethod("next");
    } catch (SecurityException e) {
      e.printStackTrace();
      return false;
    } catch (NoSuchMethodException e) {
      e.printStackTrace();
      return false;
    }

    mMsgQueueNextMethod.setAccessible(true);

    try {
      mMsgTargetFiled = clsMessage.getDeclaredField("target");
    } catch (SecurityException e) {
      e.printStackTrace();
      return false;
    } catch (NoSuchFieldException e) {
      e.printStackTrace();
      return false;
    }

    mMsgTargetFiled.setAccessible(true);
    return true;
  }

  private static void doModal() {
    isQuitModal = false;

    // get message queue associated with main UI thread
    MessageQueue queue = Looper.myQueue();
    while (!isQuitModal) {
      // call queue.next(), might block
      Message msg = null;
      try {
        msg = (Message) mMsgQueueNextMethod.invoke(queue);
      } catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
        e.printStackTrace();
      }

      if (null != msg) {
        Handler target = null;
        try {
          target = (Handler) mMsgTargetFiled.get(msg);
        } catch (IllegalArgumentException | IllegalAccessException e) {
          e.printStackTrace();
        }

        if (target == null) {
          // No target is a magic identifier for the quit message.
          isQuitModal = true;
        } else {
          target.dispatchMessage(msg);
        }
        msg.recycle();
      }
    }
  }

  /**
   * Work mode.
   */
  public enum Mode {
    /**
     * When assertion or check fails, block current thread, show dialog, and wait for user input.
     */
    DEBUG,
    /**
     * When assertion fails, log it, when check failes - throw AssertionError.
     */
    RELEASE
  }
}
