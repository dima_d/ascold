package org.mal.ascold;

import android.app.Application;
import android.content.Intent;

import org.mal.ascold.service.JoinRpgService;
import org.mal.ascold.service.MvelService;
import org.mvel2.util.MVELClassLoader;

/**
 * Main application.
 * Created by Dmitry.Subbotenko on 11.08.2016.
 */
public class AsColdApplication extends Application {
  @Override
  public void onCreate() {
    super.onCreate();
    AssertHandler.init(getApplicationContext());
    Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {

      @Override
      public void uncaughtException(Thread thread, Throwable ex) {
        if (getApplicationContext() != null) {
          AssertHandler.showAssert("GSOD", ex);
//          AssertHandler.showGsod(getApplicationContext(), ex);
        }
      }
    });


    startService(new Intent(this,JoinRpgService.class));
    startService(new Intent(this,MvelService.class));
  }
}
