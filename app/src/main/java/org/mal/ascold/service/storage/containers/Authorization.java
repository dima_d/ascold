package org.mal.ascold.service.storage.containers;

/**
 * Created by Dmitry.Subbotenko on 16.08.2016.
 */
public class Authorization {
  public Long _id; // for cupboard
  String userID;
  String sessionID;
  String name;

  public String getUserID() {
    return userID;
  }

  public String getSessionID() {
    return sessionID;
  }

  public String getName() {
    return name;
  }
}
