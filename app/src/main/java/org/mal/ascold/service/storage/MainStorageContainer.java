package org.mal.ascold.service.storage;

import org.mal.ascold.service.storage.containers.Authorization;
import org.mal.ascold.service.storage.containers.Introduction;
import org.mal.ascold.service.storage.containers.Project;
import org.mal.ascold.service.storage.containers.State;

/**
 * Created by Dmitry.Subbotenko on 19.08.2016.
 */
public class MainStorageContainer {
  private Authorization authorization;
  private Introduction introduction;
  private Project project;
  private State state;
}
