package org.mal.ascold.service.mvel;

import android.app.Activity;

import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dmitry.Subbotenko on 19.09.2016.
 */
public class ScriptExecutor {
  private static final String TAG = ScriptExecutor.class.getSimpleName();

  public void runTestScript(Activity activity) {
    try {
      FileReader reader = new FileReader(activity.getAssets().openFd("Scripts.txt").getFileDescriptor());

      Writer writer = new StringWriter();

      char[] buffer = new char[2048];
      int n;
      while ((n = reader.read(buffer)) != -1) {
        writer.write(buffer, 0, n);
      }
      String script = writer.toString();
      reader.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void runTestScript(String script) {
    Map<String, Object> input = new HashMap<String, Object>();
    //    input.put("employee", e);


  }

//      Log.d(TAG, "onSyncClick() called with " + "");
//      System.setProperty("java.version", "1.6");
//      String template = "Hello, my name is @{name.toUpperCase()}";
//      Map vars = new HashMap<>();
//      vars.put("name", "Michael");
//      Log.e(TAG, TemplateRuntime.eval(template, vars).toString());
//
//      Map<String, Object> input = new HashMap<String, Object>();
//      Employee e = new Employee();
//      e.setFirstName("john");
//      e.setLastName("michale");
//      input.put("employee", e);
          /*
           * Property Expression - Used to extract the property value out of the variable.
           */

  //    MVEL.evalFile()
//      String lastName = MVEL.evalToString("employee.getLastName()", input);
//      Log.e(TAG,"Employee Last Name:" + lastName);


private class Employee {
  private String firstName;
  private String lastName;

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getLastName() {
    return lastName;
  }
}
}
