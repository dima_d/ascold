package org.mal.ascold.service.net;

import org.mal.ascold.service.storage.containers.Authorization;
import org.mal.ascold.service.storage.containers.Introduction;
import org.mal.ascold.service.storage.containers.Project;
import org.mal.ascold.service.storage.containers.State;
import retrofit2.http.GET;
import retrofit2.http.Header;
import rx.Observable;

/**
 * Created by Dmitry.Subbotenko on 16.08.2016.
 */
public interface IJoinRpgRetrofitService {
  String BASE_URL = "http://joinrpg.ru/";
  String BASE_URL_LOCAL = "192.168.0.100/";

  @GET("users/{username}")
  Observable<Authorization> authorization(@Header("username") String username, @Header("password") String password);

  @GET("pntroduction/")
  Observable<Introduction> introduction(@Header("session_id") String session_id, @Header("project_id") String project_id);


  @GET("project/")
  Observable<Project> project();

  @GET("state/")
  Observable<State> state(@Header("session_id") String session_id, @Header("project_id") String project_id);

}
