package org.mal.ascold.service.storage;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.mal.ascold.service.storage.containers.Authorization;
import org.mal.ascold.service.storage.containers.Introduction;
import org.mal.ascold.service.storage.containers.Project;
import org.mal.ascold.service.storage.containers.State;

/**
 * Created by Dmitry.Subbotenko on 11.08.2016.
 */
public class MainStorageSQLHelper extends SQLiteOpenHelper {
  private static final String DATABASE_NAME = "mainStorage.db";
  private static final int DATABASE_VERSION = 2;

  public MainStorageSQLHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  static {
        cupboard().register(Authorization.class);
        cupboard().register(Introduction.class);
        cupboard().register(Project.class);
        cupboard().register(State.class);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    cupboard().withDatabase(db).createTables();
  }

  @Override
  
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    cupboard().withDatabase(db).upgradeTables();
  }
}
