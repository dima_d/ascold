package org.mal.ascold.service.net;

import org.mal.ascold.service.storage.containers.Authorization;
import org.mal.ascold.service.storage.containers.Introduction;
import rx.Observable;

/**
 * Created by Dmitry.Subbotenko on 16.08.2016.
 */
public interface IJoinRpgService {
  public Observable<Authorization> login(String username, String password);
  public Observable<Introduction> getIntro();
  public Authorization getCurrentAuthorization();
}
