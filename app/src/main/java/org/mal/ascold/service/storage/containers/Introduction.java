package org.mal.ascold.service.storage.containers;

/**
 * Created by Dmitry.Subbotenko on 16.08.2016.
 */
public class Introduction {
  public Long _id; // for cupboard
  private String roleId;
  private String roleName;
  private String introduction;

  public String getRoleId() {
    return roleId;
  }

  public String getRoleName() {
    return roleName;
  }

  public String getIntroduction() {
    return introduction;
  }
}
