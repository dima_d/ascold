package org.mal.ascold.service;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.sqlite.SQLiteDatabase;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import org.mal.ascold.service.storage.MainStorageSQLHelper;
import org.mal.ascold.utils.BoundService;
import org.mal.ascold.utils.ServiceConnector;
import org.mvel2.MVEL;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import rx.subjects.ReplaySubject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dmitry.Subbotenko on 21.09.2016.
 */
public class MvelService extends BoundService {

  private static final String TAG = MvelService.class.getSimpleName();


  private final ReplaySubject<Serializable> scripts = ReplaySubject.create();

  private SQLiteDatabase writableDatabase;
  private Map<String, Object> scriptSpace;

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    Log.d(TAG,
        "onStartCommand() called with " + "intent = [" + intent + "], flags = [" + flags + "], startId = [" + startId
            + "]");
    writableDatabase = new MainStorageSQLHelper(this).getWritableDatabase();

    scriptSpace = new HashMap<String, Object>();

    //    scriptSpace.put("employee", e);

    scripts.subscribe(script -> {
      MVEL.executeExpression(script,scriptSpace);
    });

    return super.onStartCommand(intent, flags, startId);
  }

  public void addScript(String script) {
    Log.d(TAG, "addScript() called with " + "script = [" + script + "]");

    Observable.combineLatest(scripts,scripts,(serializable, serializable2) -> {
      return serializable;
    });

    scripts.onNext(MVEL.compileExpression(script));
  }

  public static class Connector extends ServiceConnector<MvelService> {
    public Connector(Context context) {
      super(context, MvelService.class);
    }
  }

}
