package org.mal.ascold.service.net;

import com.google.gson.Gson;
import org.mal.ascold.service.storage.containers.Authorization;
import org.mal.ascold.service.storage.containers.Introduction;
import org.mal.ascold.service.storage.containers.Project;
import org.mal.ascold.service.storage.containers.State;
import retrofit2.http.Header;
import rx.Observable;
import rx.android.MainThreadSubscription;

/**
 * Created by Dmitry.Subbotenko on 16.08.2016.
 */
public class StubJoinRpgRetrofitService implements IJoinRpgRetrofitService {
  public static final String AUTHORIZATION = "{"
      + "\"userID\" : \"d\", "
      + "\"sessionID\" : \"100\", "
      + "\"name\" : \"Какойто гребаный ролевик\""
      + "}";
  public static final String INTRO = "{"
      + "\"roleId\" : \"1\", "
      + "\"roleName\" : \"Ричард Шарп\", "
      + "\"introduction\" : \" Пример загруза с тестовым форматированием \n"
      + "# H1\n"
      + "## H2\n"
      + "### H3\n"
      + "#### H4\n"
      + "##### H5\n"
      + "###### H6\n"
      + "\n"
      + "Alternatively, for H1 and H2, an underline-ish style:\n"
      + "\n"
      + "Alt-H1\n"
      + "======\n"
      + "\n"
      + "Alt-H2\n"
      + "------\""
      + "}";

  public static final String STATE = "{"
      + "\"roleId\" : \"1\", "
      + "\"roleName\" : \"Ричард Шарп\", "
      + "}";

  public static final String PROJECT = "{"
      + "\"id\" : \"5\", "
      + "\"projectName\" : \"Ватерлоо\", "
      + "\"startDate\" : \"01.01.2017\", "
      + "\"endDate\" : \"12.01.2017\", "
      + "}";

  Gson gson = new Gson();

  @Override
  public Observable<Authorization> authorization(@Header("username") String username,
      @Header("password") String password) {

    return Observable.create((Observable.OnSubscribe<Authorization>) subscriber -> {
      if (username.equals("d") && password.equals("d")) {
        Authorization authorization = gson.fromJson(AUTHORIZATION, Authorization.class);
        subscriber.onNext(authorization);
        subscriber.onCompleted();
      } else {
        subscriber.onError(new Exception("Not correct in stub!"));
        subscriber.onCompleted();
      }
    });

  }

  @Override
  public Observable<Introduction> introduction(@Header("session_id") String session_id,
      @Header("project_id") String project_id) {
    return Observable.create(subscriber -> {
      if (session_id.equals("100") && project_id.equals("5")) {
        Introduction introduction = gson.fromJson(INTRO, Introduction.class);
        subscriber.onNext(introduction);
        subscriber.onCompleted();
      } else {
        subscriber.onError(new Exception("Not correct in stub!"));
        subscriber.onCompleted();
      }
    });
  }

  @Override
  public Observable<Project> project() {
    return Observable.create(subscriber -> {
        subscriber.onNext(gson.fromJson(PROJECT, Project.class));
        subscriber.onCompleted();
    });  }

  @Override
  public Observable<State> state(@Header("session_id") String session_id, @Header("project_id") String project_id) {
    return Observable.create(subscriber -> {
      if (session_id.equals("100") && project_id.equals("5")) {
        subscriber.onNext(gson.fromJson(STATE, State.class));
        subscriber.onCompleted();
      } else {
        subscriber.onError(new Exception("Not correct in stub!"));
        subscriber.onCompleted();
      }
    });
  }
}
