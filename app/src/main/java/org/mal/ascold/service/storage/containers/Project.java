package org.mal.ascold.service.storage.containers;

import java.util.Date;

/**
 * Created by Dmitry.Subbotenko on 16.08.2016.
 */
public class Project {
  public Long _id; // for cupboard
  String id;
  String projectName;
  Date startDate;
  Date endDate;
}
