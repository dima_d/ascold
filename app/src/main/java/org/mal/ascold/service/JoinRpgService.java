package org.mal.ascold.service;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.sqlite.SQLiteDatabase;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import org.json.JSONException;
import org.mal.ascold.service.storage.MainStorageSQLHelper;
import org.mal.ascold.service.storage.containers.Authorization;
import org.mal.ascold.service.net.IJoinRpgRetrofitService;
import org.mal.ascold.service.net.IJoinRpgService;
import org.mal.ascold.service.storage.containers.Introduction;
import org.mal.ascold.service.net.StubJoinRpgRetrofitService;
import org.mal.ascold.utils.BoundService;
import org.mal.ascold.utils.ServiceConnector;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

/**
 * Created by Dmitry.Subbotenko on 16.08.2016.
 */
public class JoinRpgService extends BoundService implements IJoinRpgService {
  private static final String TAG = JoinRpgService.class.getSimpleName();
  private IJoinRpgRetrofitService joinRpgService;


  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    Log.d(TAG,
        "onStartCommand() called with " + "intent = [" + intent + "], flags = [" + flags + "], startId = [" + startId
            + "]");

    joinRpgService = getJoinRpgRetrofitServiceStub();

    return super.onStartCommand(intent, flags, startId);
  }

  private IJoinRpgRetrofitService getJoinRpgRetrofitService() {
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl("https://api.github.com")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
        .build();

    return retrofit.create(IJoinRpgRetrofitService.class);
  }

  private IJoinRpgRetrofitService getJoinRpgRetrofitServiceStub() {

    return new StubJoinRpgRetrofitService();
  }

  @Override
  public Observable<Authorization> login(String username, String password) {
        return joinRpgService.authorization(username,password).doOnError(new RetrofitThrowableAction());
  }

  @Override
  public Observable<Introduction> getIntro() {
    return joinRpgService.introduction("100","5"); // FIXME: this is STUB refrernce.
  }

  @Override
  public Authorization getCurrentAuthorization() {
    return null;
  }

  public static class Connector extends ServiceConnector<JoinRpgService> {
    public Connector(Context context) {
      super(context, JoinRpgService.class);
    }
  }

  private static class RetrofitThrowableAction implements Action1<Throwable> {
    @Override
    public void call(Throwable e) {
      if (e instanceof HttpException) {
        HttpException response = (HttpException) e;
        switch (response.code()) {
        case 403:
          // do nothing. It cath in user.
          break;
        default:
          e.printStackTrace();
          break;
        }
      } else if (e instanceof JSONException) {
        e.printStackTrace();
      } else {
        e.printStackTrace();
      }
    }
  }
}
